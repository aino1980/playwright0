from playwright.sync_api import expect, Page


def page_login(page: Page, username: str, password: str):
    """
    通过页面进行登录
    :param page: page对象
    :param username:
    :param password:
    :return:
    """
    # playwright = sync_playwright().start()
    # 浏览器 = playwright.chromium.launch(headless=False)
    # 上下文 = 浏览器.new_context()
    # page = 上下文.new_page()
    page.goto("https://0123456789.pingcode.com")
    page.get_by_placeholder("请输入手机号/邮箱").type(username, delay=30)
    page.get_by_placeholder("请输入登录密码").fill(password)
    page.get_by_role("button", name="登录")
    page.get_by_role("button", name="登录").click()
    expect(page.locator('.avatar-default').first).to_be_visible()
