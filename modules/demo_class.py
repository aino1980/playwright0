# 人类
class human:
    address = "earth"

    def __init__(self, sex, age, name):
        self.sex = sex
        self.age = age
        self.name = name

    def dance(self):
        print(f"{self.name} can dance")

    def jump(self):
        print(f"{self.name} can jump")

    @staticmethod
    def rap():
        print("I can rap")

    @classmethod
    def twoandhalf_year(cls):
        print("两年半的练习生")

    @property
    def add_3_age(self):
        return int(self.age) + 3


if __name__ == "__main__":
    man = human(sex="man", age="40", name="tom")
    print(man.name)
    print(man.address)
    print(man.age)
    print(man.sex)
    man.dance()
    man.jump()
    man.rap()
    human.twoandhalf_year()
    print(man.add_3_age)
