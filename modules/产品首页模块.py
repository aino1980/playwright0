from modules import *


class 产品首页(Page_Object):
    def __init__(self, page: Page):
        super().__init__(page)
        self.新建产品 = self.page.get_by_text("新建产品")
        self.产品标识 = self.page.get_by_placeholder("大写字母和数字，15 个字符以内")
        self.输入产品名称 = self.page.get_by_placeholder("输入产品名称")
        self.选择可见范围 = self.page.locator(' //thy-custom-select[@thyplaceholder="选择可见范围"]')

    def navigate(self):
        self.跳转("/ship/products")

    def create_产品(self, 产品名称):
        self.输入产品名称.fill(产品名称)
        self.选择可见范围.click()
        self.page.get_by_text("公开").click()
        self.产品标识.fill(str(time.time_ns())[-12:])
        self.page.get_by_placeholder("输入产品描述").fill("创建一个产品的测试!")
        self.page.get_by_text("下一步").click()
        self.page.get_by_text("确定").click()
        expect(self.page.get_by_text(产品名称)).to_be_visible()
