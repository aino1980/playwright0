from playwright.sync_api import sync_playwright, expect, Page, BrowserContext, Browser
from interface.page_login import page_login
from interface.page_login_save_cookie import page_login_save_cookies, 跳转


def test_pw_test(page):
    playwright = sync_playwright().start()
    浏览器 = playwright.chromium.launch(headless=False)
    上下文 = 浏览器.new_context()
    page = 上下文.new_page()
    page.goto("https://1234567890.pingcode.com/wiki/spaces")
    page.get_by_placeholder("请输入手机号/邮箱").type("30278257@qq.com", delay=30)
    page.get_by_placeholder("请输入登录密码").fill("playwright0")
    page.get_by_role("button", name="登录")
    page.get_by_role("button", name="登录").click()
    expect(page.locator('.avatar-default').first).to_be_visible()
    page_login_save_cookies(page, "mwn666888@163.com", "winni666888")
    page_login_save_cookies(page, "30278257@qq.com", "playwright0")
    跳转(page, "testhub/libraries")
    page.wait_for_timeout(5000)
