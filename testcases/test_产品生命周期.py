from interface.page_login_save_cookie import page_login_save_cookies, 跳转
from playwright.sync_api import expect, Page
import time
import pytest
产品名称 = ""


def test_创建产品(page):
    page_login_save_cookies(page, "30278257@qq.com", "playwright0")
    跳转(page, "/ship/products")
    page.get_by_text("新建产品").click()
    global 产品名称
    产品名称 = f"产品生命周期测试_{time.time_ns()}"
    page.get_by_placeholder("输入产品名称").fill(产品名称)
    page.locator(' //thy-custom-select[@thyplaceholder="选择可见范围"]').click()
    page.get_by_text("公开").click()
    page.get_by_placeholder("大写字母和数字，15字符以内").fill(str(time.time_ns())[-12:])
    page.get_by_placeholder("输入产品描述").fill("创建一个产品的测试!")
    page.get_by_text("下一步").click()
    page.get_by_text("确定").click()
    expect(page.get_by_text(产品名称)).to_be_visible()


def test_修改产品(page):
    page_login_save_cookies(page, "30278257@qq.com", "playwright0")
    跳转(page, "/ship/products")
    page.locator('//*[@name="search"]//input').fill(产品名称)
    page.keyboard.press("Enter")
    page.get_by_text(产品名称).hover()
    page.locator('//a[@thytooltip="更多"]').click()
    page.get_by_text("编辑基本信息").click()
    page.locator(' //thy-custom-select[@thyplaceholder="选择可见范围"]').click()
    page.get_by_text("私有").click()
    产品标识 = str(time.time_ns())[-12:]
    page.get_by_placeholder("大写字母和数字，15字符以内").fill(产品标识)
    page.get_by_placeholder("输入产品描述").fill("修改一个产品的测试!")
    page.get_by_text("确定").click()
    expect(page.get_by_text("修改产品成功！")).to_be_visible()
    跳转(page, "/ship/products")
    page.locator('//*[@name="search"]//input').fill(产品名称)
    page.keyboard.press("Enter")
    page.get_by_text(产品名称).hover()
    page.locator('//a[@thytooltip="更多"]').click()
    page.get_by_text("产品信息").click()
    expect(page.get_by_text("私有")).to_be_visible()
    expect(page.locator(f'//thy-tag[text()="{产品标识}"]')).to_be_visible()
    expect(page.get_by_text("修改一个产品的测试!")).to_be_visible()


def test_删除产品(page, 删除所有产品生命周期测试_的数据):
    page_login_save_cookies(page, "30278257@qq.com", "playwright0")
    跳转(page, "/admin/product/ship/configuration/products")
    page.locator('//*[@name="search"]//input').fill(产品名称)
    page.keyboard.press("Enter")
    page.locator("#axy删除").click()
    expect(page.get_by_text("删除后如果想找回该产品，可以通过配置中心由管理员进行恢复。")).to_be_visible()
    page.get_by_text("确定").click()
    expect(page.get_by_text("删除成功")).to_be_visible()
    expect(page.get_by_text("暂无数据")).to_be_visible()


@pytest.fixture()
def 删除所有产品生命周期测试_的数据(page: Page):
    yield
    page_login_save_cookies(page, "30278257@qq.com", "playwright0")
    跳转(page, "/admin/product/ship/configuration/products")
    page.locator('//*[@name="search"]//input').fill("产品生命周期测试_")
    page.keyboard.press("Enter")
    while True:
        try:
            page.get_by_text("暂无数据").wait_for(timeout=3000)
            break
        except:
            page.locator("#axy删除").first.click()
            page.get_by_text("确定").click()
            expect(page.get_by_text("删除成功")).to_be_visible()




