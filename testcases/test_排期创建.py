from interface.page_login_save_cookie import page_login_save_cookies, 跳转
from playwright.sync_api import expect, Page
import time
from utils.tools import 日期控件填写
import pytest


def test_排期创建(page):
    page_login_save_cookies(page, "30278257@qq.com", "playwright0")
    跳转(page, "/ship/products")
    page.get_by_text("测试专用").click()
    page.get_by_text("排期").click()
    page.get_by_text("新建计划").click()
    计划名称 = f"计划名称_{time.time_ns()}"
    page.get_by_placeholder("输入计划名称").fill(计划名称)
    日期控件填写(page, page.get_by_placeholder("选择开始时间"), 0)
    日期控件填写(page, page.get_by_placeholder("选择结束时间"), 300)
    page.get_by_text("确定").click()
    expect(page.locator("tr").filter(has_text=计划名称))

