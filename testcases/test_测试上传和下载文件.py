from interface.page_login_save_cookie import page_login_save_cookies, 跳转
from playwright.sync_api import expect, Page
import time
from utils.tools import 日期控件填写, get_path
import pytest


def test_排期创建(page):
    page_login_save_cookies(page, "30278257@qq.com", "playwright0")
    跳转(page, "/ship/products")
    page.get_by_text("测试专用").click()
    page.get_by_text("工单").click()
    while True:
        try:
            page.locator('//a[@thytooltip="更多"]').click(timeout=1000)
            page.get_by_text("导入工单").click(timeout=1000)
            break
        except:
            pass
    with page.expect_download() as f:
        page.locator("a").get_by_text("下载模板").click()
    file_path = get_path(f"download/{time.time_ns()}.xlsx")
    f.value.save_as(file_path)
    page.set_input_files('//input[@type="file"]', get_path("upload/testupload.xlsx"))
    expect(page.get_by_text("testupload.xlsx")).to_be_visible()
    page.get_by_text("取消").click()
    page.locator('//span[text()="客户"]').click()
    page.get_by_text("新建").click()
    with page.expect_file_chooser() as f:
        page.get_by_text("点击上传").click()
    f.value.set_files(get_path("upload/条件.png"))
    page.get_by_text("确定").last.click()
    page.wait_for_timeout(100000)



