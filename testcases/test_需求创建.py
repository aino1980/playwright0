from interface.page_login_save_cookie import page_login_save_cookies, 跳转
from playwright.sync_api import expect, Page
import time
import pytest


def test_需求创建(page):
    page_login_save_cookies(page, "30278257@qq.com", "playwright0")
    跳转(page, "/ship/products")
    page.get_by_text("测试专用").click()
    page.get_by_text("新建需求").click()
    需求标题 = f"新建需求测试_{time.time_ns()}"
    page.get_by_placeholder("输入标题").fill(需求标题)
    page.locator(".thy-icon-bold").click()  # 黑体的点击
    page.locator(".thy-icon-italic").click()  # 斜体的点击
    page.locator(".slate-editable-container").click()
    page.keyboard.type("测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写")
    page.keyboard.press("Shift+ArrowLeft")
    page.keyboard.press("Shift+ArrowLeft")
    page.locator(".thy-icon-bold").click()  # 黑体的点击
    page.locator(".thy-icon-italic").click()  # 斜体的点击
    expect(page.locator("span").filter(has_text="测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文").first).to_have_attribute("the-bold", "true")
    expect(page.locator("span").filter(has_text="测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文填写测试需求正文").first).to_have_attribute("the-italic", "true")
    expect(page.locator("span").filter(has=page.locator('//span[text()="填写"]')).first).not_to_have_attribute("the-bold", "true")
    expect(page.locator("span").filter(has=page.locator('//span[text()="填写"]')).first).not_to_have_attribute("the-italic", "true")
    page.get_by_text("确定").click()
    expect(page.locator("tr").filter(has_text="待评审").filter(has_text=需求标题)).to_be_visible()